# pgn2gif
Generate gifs from your chess games.
Just put a pgn file to script directory and run it. 


# Example
## PGN
```
1. e4 e5 2. Qh5 Ke7 3. Qxe5#
```

## GIF output
<img src="https://media.giphy.com/media/2UtkKmkhBCfv0bXHBk/giphy.gif">
